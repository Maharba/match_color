# match_color

This is just a sample project, a mini game for children where you have to match the name of the color with the actual color of the panel.

![Sample](/assets/screen2.png)