import 'dart:math';
import 'package:flutter/material.dart';

void main() => runApp(MatchyFun());

class MatchyFun extends StatelessWidget {
  const MatchyFun({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MatchyFunHome(),
    );
  }
}

class MatchyFunHome extends StatefulWidget {
  MatchyFunHome({Key key}) : super(key: key);

  @override
  _MatchyFunState createState() => _MatchyFunState();
}

class _MatchyFunState extends State<MatchyFunHome> {
  int colorsName = generateRandomColor();

  static int generateRandomColor() {
    List<int> colors = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    return colors[Random().nextInt(10)];
  }

  Future<void> _showMyDialog(String title, String content, String buttonText) async {    
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  String _getColorName() {
    String colorsNameText;

    switch (colorsName) {
      case 0:
        colorsNameText = "pink";
        break;
      case 1:
        colorsNameText = "indigo";
        break;
      case 2:     
        colorsNameText = "grey";
        break;
      case 3:
        colorsNameText = "green";
        break;
      case 4:     
        colorsNameText = "yellow";
        break;
      case 5:
        colorsNameText = "cyan";
        break;
      case 6:     
        colorsNameText = "orange";
        break;
      case 7:
        colorsNameText = "teal";
        break;
      case 8:     
        colorsNameText = "purple";
        break;
      default:
        colorsNameText = "";
        break;
    }
    return colorsNameText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Matchy Fun'),
        actions: <Widget>[
          FlatButton(onPressed: () {
            setState(() {
              colorsName = generateRandomColor();
            });
          }, child: Text('Reset', style: TextStyle(color: Colors.white)))
        ],
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Text(
                _getColorName(),
                style: TextStyle(fontSize: 72, fontWeight: FontWeight.bold),
              ),
            )
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 3,
              children: <Widget>[
                Container(
                  color: Colors.pink,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 0) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    }),
                ),
                Container(
                  color: Colors.indigo,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 1) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.blueGrey,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 2) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.green,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 3) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.amber,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 4) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.lightBlue,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 5) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.orange,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 6) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.teal,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 7) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
                Container(
                  color: Colors.purple,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (colorsName == 8) {
                          _showMyDialog("Congratulations", "You won!", "Ok");
                        }
                        else {
                          _showMyDialog("Sorry", "Trye next time", "Ok");
                        }
                      });
                    },
                  ),
                ),
              ],
            )
          )
        ],
      )
    );
  }
}
